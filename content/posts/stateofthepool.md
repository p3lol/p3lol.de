+++
title = "State of the Pool and Activities"
description = "What are we doing, where are wo going?"
type = ["posts","post"]
tags = [
    "p3lol",
    "pool",
    "community",
    "gitlab",
    "blocks",
    "donations",
    "internals",
]
date = "2021-10-23"
categories = [
    "Cardano",
    "Pool",
]
series = ["Updates"]
[ author ]
  name = "p3lol Team"
+++

We thought it might be time to update everyone on what's going on with the pool
and with the people behind it. We'll do this more regular from now on just to provide
a bit more transparency to everyone – all in all, we did set out to use p3lol “for good”,
which should happen in an open manner so anyone can see what is going on.

## Deployment / Tech

We strive to keep the pool running 24/7 and to keep it up to date as much as possible.
We've switched to new `cardano-node` versions within only a few days after every new release,
and we've been at [1.30.1](https://github.com/input-output-hk/cardano-node/releases/tag/1.30.1) since 2021-10-02, for example

A slightly modified `topologyUpdater` script is used to make sure the node keeps connected to
a sufficient amount of other nodes. The state of the pool, topology updates, backups etc.
is continuously monitored, so any issue will be quickly reported (using matrix webhooks and
a separate healtchecks.io instance).

The slightly increased performance requirements coming with some of the recent `cardano-node`
updates have never been an issue for our servers – there is still a lot of capacity left,
so it can be assumed there will be no such problems for quite some time in the future.

Our relay node runs on a [VPS 3000 G9](https://www.netcup.de/vserver/vps.php), our block producing node runs on an [RS 2000 G9](https://www.netcup.de/vserver/); both at netcup.

## Blocks

Our pool has produced four blocks to date. Unfortunately, the first one was lost due to improper registration
of the pool reward address (which is kept as a separate account on a hardware wallet).
The other three were successfully claimed. It has proven surprisingly difficult do get a consensus on worthy
organizations to donate rewards to, which is why most of the rewards (except for amounts paid out to the owners directly)
are still unused and remaining in the wallet.

## Donations

As explained in the previous paragraph, donations just haven't happened yet.
We hope we can streamline this soon by setting up an easier way for owners to vote on organizations
to donate to – we'll keep you updated on any such developments!

## Community / Open Source

We've already made our website's source available at https://gitlab.com/p3lol/p3lol.de and provide
a few specific packages for Arch users at https://gitlab.com/p3lol/cardano-pkgbuilds.

Our next step will be to also make our docker setup available (including relevant firewall rules, grafana
integration etc.) so anyone can benefit from our work. We'll intentionally not provide it as a
“simple script” to “fire and forget” – instead, we'll try to document what we're doing and why, so anyone
using our setup as a starting point will be able to understand what is going on and why.
This is very relevant, especially when it comes to setting things up securely and reliably.

We'll announce this on here, as soon as it's made available on [our gitlab account](https://gitlab.com/p3lol/).
