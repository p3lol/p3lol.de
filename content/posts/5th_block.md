+++
title = "5th block"
description = "Minty"
type = ["posts","post"]
tags = [
    "p3lol",
    "pool",
    "blocks",
    "donations",
]
date = "2021-11-08"
categories = [
    "Cardano",
    "Pool",
]
series = ["Updates"]
[ author ]
  name = "p3lol Team"
+++

## Only the freshest of blocks

Just a quick update: We've minted our [5th block](https://cardanoscan.io/block/6467760) in Epoch 300!
