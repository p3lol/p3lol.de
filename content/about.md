+++
title = "About our Pool"
+++

The P3LOL pool is collectively owned and operated by several members of the [mods.de][mods.de]-Community.

We aim to use part of the pool rewards to support the continued existence of this community and, if possible, donate to further charitable causes and organizations. This will include the OffTopic e.V (an organization to support the [mods.de][mods.de]-Community) as well as humanitarian organizations and organizations supporting privacy and decentralization.

We will soon specify our intended recipients and causes to donate to and announce any donations made on this website to show our commitment to this idea.

Our hardened and fully encrypted servers are hosted in data centers in Germany.

Currently we have one block producing node and one relay node. Should our pool grow as we hope it will, we intend to provide more relay nodes and consider setting up a fallback block producing node for highest availability.

Find our pool on:
* [pool.pm][poolpm]
* [Cardanoscan][cardanoscan]
* [ADApools][adapools]
* [AdaStat][adastat]
* [PoolTool][pooltool]

Our pool ID is `bb236318e9fe9f7b2e287dd388f9cbd32cb2e3ead404684c720c91b6`.

[mods.de]: https://forum.mods.de/bb/board.php?BID=14
[poolpm]: https://pool.pm/bb236318e9fe9f7b2e287dd388f9cbd32cb2e3ead404684c720c91b6
[cardanoscan]: https://cardanoscan.io/pool/bb236318e9fe9f7b2e287dd388f9cbd32cb2e3ead404684c720c91b6
[adapools]: https://adapools.org/pool/bb236318e9fe9f7b2e287dd388f9cbd32cb2e3ead404684c720c91b6
[adastat]: https://adastat.net/pools/bb236318e9fe9f7b2e287dd388f9cbd32cb2e3ead404684c720c91b6
[pooltool]: https://pooltool.io/pool/bb236318e9fe9f7b2e287dd388f9cbd32cb2e3ead404684c720c91b6
